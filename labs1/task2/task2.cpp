﻿#include <iostream>

using namespace std;

union n
{
    signed short a;
    struct num
    {
        unsigned short x0 : 1;
        unsigned short x1 : 1;
        unsigned short x2 : 1;
        unsigned short x3 : 1;
        unsigned short x4 : 1;
        unsigned short x5 : 1;
        unsigned short x6 : 1;
        unsigned short x7 : 1;
        unsigned short x8 : 1;
        unsigned short x9 : 1;
        unsigned short x10 : 1;
        unsigned short x11 : 1;
        unsigned short x12 : 1;
        unsigned short x13 : 1;
        unsigned short x14 : 1;
        unsigned short x15 : 1;
    }x;
}z;

void main()
{
    signed short x;
    cout << "Enter count: ";  cin >> x;
    cout << "1. Data structures and associations" << endl;
    z.a = x;
    z.x.x15 == 1 ? cout << "Number sign: - " << endl : cout << "Number sign: +" << endl;
    cout << "2. Bitwise logical operations" << endl;
    (z.x.x15 != 0 && z.x.x15 > 0) ? cout << "Number sign: - " << endl : cout << "Number sign: +" << endl;
}

