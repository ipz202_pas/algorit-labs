﻿#include <iostream>
#include <time.h>

struct date
{
	unsigned short WeekDay : 3;
	unsigned short MonthDay : 6;
	unsigned short Month : 4;
	unsigned short Year : 11;
	unsigned short Hours : 5;
	unsigned short Minutes : 6;
	unsigned short Seconds : 6;
};


int main()
{
	date d = { 01, 01, 01, 1999, 12, 30, 00 };
	printf("Date: Hour: %d, \nMinutes: %d, \nSecond: %d, \nYear: %d, \nMonth: %d, \nMonthDay: %d, \nWeekday: %d\n", d.Hours, d.Minutes, d.Seconds, d.Year, d.Month, d.MonthDay, d.WeekDay);
	return 0;
}
