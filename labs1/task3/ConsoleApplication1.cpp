﻿#include <stdio.h>

int main()
{
    signed char a, b, c, d, e, f;
    a = 5; b = 127;
    c = a + b;
    printf("a) c = 5 + 127 = %d\n", c);
    a = 2; b = 3;
    c = a - b;
    printf("b) c = 2 - 3 = %d\n", c);
    a = -120; b = 34;
    c = a - b;
    printf("c) c = -120 - 34 = %d\n", c);
    unsigned char x;
    x = (unsigned char)(-5);
    printf("d) c = (unsigned char) -5 = %d\n", x);
    c = 56 & 38;
    printf("e) c = 56 & 38 = %d\n", c);
    c = 56 | 38;
    printf("f) c = 56 | 38 = %d\n", c);
    return 0;
}
