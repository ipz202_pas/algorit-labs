﻿using System;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace Sorting
{
    class Program
    {
        public class HeapSort
        {
            public void sort(float[] arr)
            {
                int n = arr.Length;

                // Побудова кучі (перегрупування масиву)
                for (int i = n / 2 - 1; i >= 0; i--)
                    heapify(arr, n, i);

                // По черзі витягуєм усі елементи кучі
                for (int i = n - 1; i >= 0; i--)
                {
                    // Переміщуємо поточний корінь на кінець
                    float temp = arr[0];
                    arr[0] = arr[i];
                    arr[i] = temp;

                    // викликаємо процедуру heapify на вже зменшенній кучі
                    heapify(arr, i, 0);
                }
            }

            //Процедура для перетворення в двійкову кучу піддерева з корневим вузлом, який являє
            // собою індекс в масиві

            void heapify(float[] arr, int n, int i)
            {
                int largest = i;
                // Ініціалізуємо невеликий елемент як корінь
                int left = 2 * i + 1;
                int right = 2 * i + 2;

                // Якщо лівий дочірній елемент більше заданого кореня
                if (left < n && arr[left] > arr[largest])
                    largest = left;

                // Якщо правий дочірній елемент більше за найбільший на данний момент еле-мент
                if (right < n && arr[right] > arr[largest])
                    largest = right;

                // Якщо найбільший елемент не є коренем
                if (largest != i)
                {
                    float swap = arr[i];
                    arr[i] = arr[largest];
                    arr[largest] = swap;

                    //За допомогою рекурсії перетворюємо у двійкову кучу вищенаведене піддерево
                    heapify(arr, n, largest);
                }
            }
        }
        public class ShellSort
        {
            public void sort(double[] array)
            {
                int i, j, step;
                double tmp;
                //Papernov & Stasevich increment, який використовується для пришвидчення алгоритму
                //Відбувається сортування включеннями з відстаннями, що зменшуються
                for (step = 1; step > 0; step = 2 ^ step + 1)
                    for (i = step; i < array.Length; i++)
                    {
                        tmp = array[i];
                        for (j = i; j >= step; j -= step)
                        {
                            if (tmp < array[j - step])
                                array[j] = array[j - step];
                            else
                                break;
                        }
                        array[j] = tmp;
                    }
            }
        }

        public class CountingSort
        {
            public void sort(short[] arr1)
            {
                int max = arr1.Max();
                int min = arr1.Min();
                int range = max - min + 1;
                //Підрахунок входженя кожного елемента
                short[] count = new short[range];
                short[] output = new short[arr1.Length];
                //Відсортовуємо масив, спираючись на кількість входжень
                for (int i = 0; i < arr1.Length; i++)
                {
                    count[arr1[i] - min]++;
                }
                for (int i = 1; i < count.Length; i++)
                {
                    count[i] += count[i - 1];
                }
                for (int i = arr1.Length - 1; i >= 0; i--)
                {
                    output[count[arr1[i] - min] - 1] = arr1[i];
                    count[arr1[i] - min]--;
                }
                for (int i = 0; i < arr1.Length; i++)
                {
                    arr1[i] = output[i];
                }
            }
        }

        static void Main(string[] args)
        {
            System.Globalization.CultureInfo customCulture = System.Globalization.CultureInfo ; System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;

            Stopwatch st;
            Random rand = new Random();

            Console.WriteLine("Пірамідальне сортування(сортування кучею):");
            Console.Write("\n");
            HeapSort obj = new HeapSort();
            float[] arr = new float[10];
            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = (float)rand.Next(100, 1000) / 10f;
            }
            st = Stopwatch.StartNew();
            obj.sort(arr);
            st.Stop();
            Console.WriteLine($"Час сортування масиву на 10 елементів - {st.Elapsed.TotalMilliseconds} ms");
            arr = new float[100];
            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = (float)rand.Next(100, 1000) / 10f;
            }
            st = Stopwatch.StartNew();
            obj.sort(arr);
            st.Stop();
            Console.WriteLine($"Час сортування масиву на 100 елементів - {st.Elapsed.TotalMilliseconds} ms");
            arr = new float[500];
            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = (float)rand.Next(100, 1000) / 10f;
            }
            st = Stopwatch.StartNew();
            obj.sort(arr);
            st.Stop();
            Console.WriteLine($"Час сортування масиву на 500 елементів - {st.Elapsed.TotalMilliseconds} ms");
            arr = new float[1000];
            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = (float)rand.Next(100, 1000) / 10f;
            }
            st = Stopwatch.StartNew();
            obj.sort(arr);
            st.Stop();
            Console.WriteLine($"Час сортування масиву на 1000 елементів - {st.Elapsed.TotalMilliseconds} ms");
            arr = new float[2000];
            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = (float)rand.Next(100, 1000) / 10f;
            }
            st = Stopwatch.StartNew();
            obj.sort(arr);
            st.Stop();
            Console.WriteLine($"Час сортування масиву на 2000 елементів - {st.Elapsed.TotalMilliseconds} ms");
            arr = new float[5000];
            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = (float)rand.Next(100, 1000) / 10f;
            }
            st = Stopwatch.StartNew();
            obj.sort(arr);
            st.Stop();
            Console.WriteLine($"Час сортування масиву на 5000 елементів - {st.Elapsed.TotalMilliseconds} ms");
            arr = new float[10000];
            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = (float)rand.Next(100, 1000) / 10f;
            }
            st = Stopwatch.StartNew();
            obj.sort(arr);
            st.Stop();
            Console.WriteLine($"Час сортування масиву на 10000 елементів - {st.Elapsed.TotalMilliseconds} ms");
            Console.WriteLine("\n");
            Console.WriteLine("Сортування Шелла:");
            Console.Write("\n");
            ShellSort ob = new ShellSort();
            double[] array = new double[10];
            double max = 100;
            double min = 0;
            for (int i = 0; i < array.Length; i++)
            {
                array[i] = rand.NextDouble() * (max - min) + min;
            }
            st = Stopwatch.StartNew();
            ob.sort(array);
            st.Stop();
            Console.WriteLine($"Час сортування масиву на 10 елементів - {st.Elapsed.TotalMilliseconds} ms");
            array = new double[100];
            for (int i = 0; i < array.Length; i++)
            {
                array[i] = rand.NextDouble() * (max - min) + min;
            }
            st = Stopwatch.StartNew();
            ob.sort(array);
            st.Stop();
            Console.WriteLine($"Час сортування масиву на 100 елементів - {st.Elapsed.TotalMilliseconds} ms");
            array = new double[500];
            for (int i = 0; i < array.Length; i++)
            {
                array[i] = rand.NextDouble() * (max - min) + min;
            }
            st = Stopwatch.StartNew();
            ob.sort(array);
            st.Stop();
            Console.WriteLine($"Час сортування масиву на 500 елементів - {st.Elapsed.TotalMilliseconds} ms");
            array = new double[1000];
            for (int i = 0; i < array.Length; i++)
            {
                array[i] = rand.NextDouble() * (max - min) + min;
            }
            st = Stopwatch.StartNew();
            ob.sort(array);
            st.Stop();
            Console.WriteLine($"Час сортування масиву на 1000 елементів - {st.Elapsed.TotalMilliseconds} ms");
            array = new double[2000];
            for (int i = 0; i < array.Length; i++)
            {
                array[i] = rand.NextDouble() * (max - min) + min;
            }
            st = Stopwatch.StartNew();
            ob.sort(array);
            st.Stop();
            Console.WriteLine($"Час сортування масиву на 2000 елементів - {st.Elapsed.TotalMilliseconds} ms");
            array = new double[5000];
            for (int i = 0; i < array.Length; i++)
            {
                array[i] = rand.NextDouble() * (max - min) + min;
            }
            st = Stopwatch.StartNew();
            ob.sort(array);
            st.Stop();
            Console.WriteLine($"Час сортування масиву на 5000 елементів - {st.Elapsed.TotalMilliseconds} ms");
            array = new double[10000];
            for (int i = 0; i < array.Length; i++)
            {
                array[i] = rand.NextDouble() * (max - min) + min;
            }
            st = Stopwatch.StartNew();
            ob.sort(array);
            st.Stop();
            Console.WriteLine($"Час сортування масиву на 10000 елементів - {st.Elapsed.TotalMilliseconds} ms");
            Console.WriteLine("\n");

            Console.WriteLine("Сортування підрахунком:");
            Console.Write("\n");
            CountingSort obje = new CountingSort();
            short[] arr1 = new short[10];
            for (int i = 0; i < arr1.Length; i++)
            {
                arr1[i] = (short)rand.Next();
            }
            st = Stopwatch.StartNew();
            obje.sort(arr1);
            st.Stop();
            Console.WriteLine($"Час сортування масиву на 10 елементів - {st.Elapsed.TotalMilliseconds} ms");
            arr1 = new short[100];
            for (int i = 0; i < arr1.Length; i++)
            {
                arr1[i] = (short)rand.Next(-100, 10);
            }
            st = Stopwatch.StartNew();
            obje.sort(arr1);
            st.Stop();
            Console.WriteLine($"Час сортування масиву на 100 елементів - {st.Elapsed.TotalMilliseconds} ms");
            arr1 = new short[500];
            for (int i = 0; i < arr1.Length; i++)
            {
                arr1[i] = (short)rand.Next(-100, 10);
            }
            st = Stopwatch.StartNew();
            obje.sort(arr1);
            st.Stop();
            Console.WriteLine($"Час сортування масиву на 500 елементів - {st.Elapsed.TotalMilliseconds} ms");
            arr1 = new short[1000];
            for (int i = 0; i < arr1.Length; i++)
            {
                arr1[i] = (short)rand.Next(-100, 10);
            }
            st = Stopwatch.StartNew();
            obje.sort(arr1);
            st.Stop();
            Console.WriteLine($"Час сортування масиву на 1000 елементів - {st.Elapsed.TotalMilliseconds} ms");
            arr1 = new short[2000];
            for (int i = 0; i < arr1.Length; i++)
            {
                arr1[i] = (short)rand.Next(-100, 10);
            }
            st = Stopwatch.StartNew();
            obje.sort(arr1);
            st.Stop();
            Console.WriteLine($"Час сортування масиву на 2000 елементів - {st.Elapsed.TotalMilliseconds} ms");
            arr1 = new short[5000];
            for (int i = 0; i < arr1.Length; i++)
            {
                arr1[i] = (short)rand.Next(-100, 10);
            }
            st = Stopwatch.StartNew();
            obje.sort(arr1);
            st.Stop();
            Console.WriteLine($"Час сортування масиву на 5000 елементів - {st.Elapsed.TotalMilliseconds} ms");
            arr1 = new short[10000];
            for (int i = 0; i < arr1.Length; i++)
            {
                arr1[i] = (short)rand.Next(-100, 10);
            }
            st = Stopwatch.StartNew();
            obje.sort(arr1);
            st.Stop();
            Console.WriteLine($"Час сортування масиву на 10000 елементів - {st.Elapsed.TotalMilliseconds} ms");
            Console.ReadKey();
        }
    }
}
