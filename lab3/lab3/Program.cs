﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;

public delegate double MyFunc(double function);

namespace Algo
{
    public class Task_1
    {
        public static void Tabulate(int a, int b, int h, MyFunc func)
        {
            for (int i = a; i <= b; i += h)
            {
                double y = func(i);

                if (y > 500) return;

                WriteLine($"f({i}) = {y:F3}");
            }
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            int a = 0, b = 50, h = 1;
            int menu; bool flag = true;









            WriteLine("Виберіть функцію:");
            WriteLine("1) f(n)=n;");
            WriteLine("2) f(n)=log(n);");
            WriteLine("3) f(n)=n*log(n);");
            WriteLine("4) f(n)=n^2;");
            WriteLine("5) f(n)=2^n;");
            WriteLine("6) f(n)=n!;");

            do
            {
                flag = int.TryParse(ReadLine(), out menu);

                if (!flag || menu > 6)
                    WriteLine("Помилка!");

            } while (!flag || menu > 6);

            switch (menu)
            {
                case 1:
                    Task_1.Tabulate(a, b, h, (x) => x);
                    break;

                case 2:
                    Task_1.Tabulate(a, b, h, (x) => Math.Log(x));
                    break;

                case 3:
                    Task_1.Tabulate(a, b, h, (x) => x * Math.Log(x));
                    break;

                case 4:
                    Task_1.Tabulate(a, b, h, (x) => Math.Pow(x, 2));
                    break;

                case 5:
                    Task_1.Tabulate(a, b, h, (x) => Math.Pow(2, x));
                    break;

                case 6:
                    Task_1.Tabulate(a, b, h, (x) =>
                    {
                        double fact = 1;
                        for (double r = x; r > 0; r--)
                            fact *= r;
                        return fact;
                    });
                    break;
            }
        }
    }
}

