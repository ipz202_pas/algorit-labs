﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;

namespace Algo
{
    public class Task_2
    {
        public static void Fibonacci(int n)
        {
            int first = 0, second = 1, sum = 0;

            Write("{0} ", first);
            Write("{0} ", second);

            for (int i = n; i > 1; i--)
            {
                sum = first + second;

                Write("{0} ", sum);

                first = second;
                second = sum;
            }
            WriteLine();
            WriteLine($"{n} номер фібоначі = {sum}");
        }
        public static void Sort(float[] arr)
        {
            float tmp = 0;
            for (int i = 0; i < arr.Length; i++)
            {
                for (int j = 0; j < arr.Length - 1; j++)
                {
                    if (arr[j] < arr[j + 1])
                    {
                        tmp = arr[j];
                        arr[j] = arr[j + 1];
                        arr[j + 1] = tmp;
                    }
                }
            }
        }
        public static void WriteArr(float[] arr)
        {
            Write("Arr - {");
            for (int i = 0; i < arr.Length; i++)
            {
                if ((i + 1) % 50 == 0)
                {
                    Write($"{arr[i]}");
                    if (i < arr.Length - 1)
                        Write(", ");
                }
            }
            WriteLine("}");
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            int n; bool flag = true;
            do
            {
                Write("n - ");

                flag = int.TryParse(ReadLine(), out n);

                if (!flag || n > 90 || n < 2)
                    WriteLine("Помилка!");

            } while (!flag || n > 90 || n < 2);


            Task_2.Fibonacci(n);


            WriteLine();
            WriteLine();

            float[] arr = new float[1000];
            Random random = new Random();
            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = random.Next(-200, 210);
                arr[i] /= 10;
            }

            Task_2.WriteArr(arr);

            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            Task_2.Sort(arr);

            stopwatch.Stop();

            TimeSpan ts = stopwatch.Elapsed;
            string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
                ts.Hours, ts.Minutes, ts.Seconds,
                ts.Milliseconds / 10);
            WriteLine("RunTime " + elapsedTime);

            WriteLine();
            WriteLine();
            Task_2.WriteArr(arr);

        }
    }
}

