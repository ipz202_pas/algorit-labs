﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.Diagnostics;
namespace Sorting
{
    public class Node<T>
    {
        public Node(T data)
        {
            Data = data;
        }
        public T Data { get; set; }
        public Node<T> Previous { get; set; }
        public Node<T> Next { get; set; }
    }
    public class LinkedList<T> : IEnumerable<T>
    {
        Node<T> head;
        Node<T> tail;
        int count;
        public void Add(T data)
        {
            Node<T> node = new Node<T>(data);
            if (head == null)
                head = node;
            else
            {
                tail.Next = node;
                node.Previous = tail;
            }
            tail = node;
            count++;
        }
        public void Add(Node<T> node)
        {
            if (head == null)
                head = node;
            else
            {
                tail.Next = node;
                node.Previous = tail;
            }
            tail = node;
            count++;
        }
        public void AddFirst(T data)
        {
            Node<T> node = new Node<T>(data);
            Node<T> temp = head;
            node.Next = temp;
            head = node;
            if (count == 0)
                tail = head;
            else
                temp.Previous = node;
            count++;
        }

        public void Remove(Node<T> data)
        {
            Node<T> current = head;
            while (current != null)
            {
                if (current == data)
                    break;
                current = current.Next;
            }
            if (current != null)
            {
                if (current.Next != null)
                    current.Next.Previous = current.Previous;
                else
                    tail = current.Previous;
                if (current.Previous != null)
                    current.Previous.Next = current.Next;
                else
                    head = current.Next;
                count--;
            }
        }
        public Node<T> GetTail()
        {
            return tail;
        }
        public Node<T> GetHead()
        {
            return head;
        }
        public void SetHead(Node<T> node)
        {
            Node<T> temp = head;
            node.Next = temp;
            head = node;
            temp.Previous = node;
        }
        public int Count { get { return count; } }
        public bool IsEmpty { get { return count == 0; } }
        public void Clear()
        {
            head = null;
            tail = null;
            count = 0;
        }
        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable)this).GetEnumerator();
        }
        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            Node<T> current = head;
            while (current != null)
            {
                yield return current.Data;
                current = current.Next;
            }
        }

    }
    class Program
    {
        static void selectionList(Node<int> head)
        {
            Node<int> temp = head;
            // Проходження по списку
            while (temp != null)
            {
                Node<int> min = temp;
                Node<int> after = temp.Next;
                // Обхід несортованого підсписку
                while (after != null)
                {
                    if (min.Data > after.Data)
                        min = after;

                    after = after.Next;
                }
                // Обмін елементів
                int x = temp.Data;
                temp.Data = min.Data;
                min.Data = x;
                temp = temp.Next;
            }
        }
        static void insertionArray(int[] arr)
        {
            //Перебір елементів у невідсортованій частині масиву
            for (int i = 1; i < arr.Length; i++)
            {
                int j;
                int tmp = arr[i];
                //Кожен елемент вставляється у відсортовану частину масиву та впорядко-вується таким чином
                for (j = i - 1; j >= 0; j--)
                {
                    if (arr[j] < tmp)
                        break;
                    arr[j + 1] = arr[j];
                }
                // ставимо tmp перед знайденим елементом
                arr[j + 1] = tmp;
            }
        }

        public class LinkedListSecond
        {
            public node head;
            public node sorted;
            int count;
            public class node
            {
                public int val;
                public node next;
                public node(int val)
                {
                    this.val = val;
                }
            }
            //Вставка елементів у список
            public void push(int val)
            {
                node newnode = new node(val);
                newnode.next = head;
                head = newnode;
            }
            public void insertionList(node headref)
            {
                //Створюємо початковий список
                sorted = null;
                node current = headref;
                //Проходження по списку та внесення current у відсортований список
                while (current != null)
                {
                    node next = current.next;
                    sortedInsert(current);
                    current = next;
                }
                // змінюємо посилання на перший елемент
                head = sorted;
            }
            // метод для вставки елемента в список
            public void sortedInsert(node newnode)
            {
                // Перенесення елемента список у початок, якщо він є min
                if (sorted == null || sorted.val >= newnode.val)
                {
                    newnode.next = sorted;
                    sorted = newnode;
                }
                else
                {
                    node current = sorted;
                    // Пошук елемента, перед яким потібрно вставити вузол списку
                    while (current.next != null &&
                            current.next.val < newnode.val)
                    {
                        current = current.next;
                    }
                    //Зміна посилань на сусдні елементи від знайденого
                    newnode.next = current.next;
                    current.next = newnode;
                }
            }
            public void сlear()
            {
                head = null;
                count = 0;
            }
        }
        static void Main(string[] args)
        {
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo); System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;
            Stopwatch st;
            Console.WriteLine("Сортування вибором (список)");
            Console.WriteLine("\n");
            LinkedList<int> linkedList = new LinkedList<int>();
            Node<int> head;
            Random rand = new Random();
            for (int i = 0; i < 10; i++)
            {
                int newNode = rand.Next();
                linkedList.Add(newNode);
            }
            head = linkedList.GetHead();
            st = Stopwatch.StartNew();
            selectionList(head);
            st.Stop();
            Console.WriteLine($"Час сортування списку на 10 елементів - {st.Elapsed.TotalMilliseconds}");
            linkedList.Clear();
            for (int i = 0; i < 100; i++)
            {
                int newNode = rand.Next();
                linkedList.Add(newNode);
            }
            head = linkedList.GetHead();
            st = Stopwatch.StartNew();
            selectionList(head);
            st.Stop();
            Console.WriteLine($"Час сортування списку на 100 елементів - {st.Elapsed.TotalMilliseconds}");
            linkedList.Clear();
            for (int i = 0; i < 500; i++)
            {
                int newNode = rand.Next();
                linkedList.Add(newNode);
            }
            head = linkedList.GetHead();
            st = Stopwatch.StartNew();
            selectionList(head);
            st.Stop();
            Console.WriteLine($"Час сортування списку на 500 елементів - {st.Elapsed.TotalMilliseconds}");
            linkedList.Clear();
            for (int i = 0; i < 1000; i++)
            {
                int newNode = rand.Next();
                linkedList.Add(newNode);
            }
            head = linkedList.GetHead();
            st = Stopwatch.StartNew();
            selectionList(head);
            st.Stop();
            Console.WriteLine($"Час сортування списку на 1000 елементів - {st.Elapsed.TotalMilliseconds}");
            linkedList.Clear();
            for (int i = 0; i < 2000; i++)
            {
                int newNode = rand.Next();
                linkedList.Add(newNode);
            }
            head = linkedList.GetHead();
            st = Stopwatch.StartNew();
            selectionList(head);
            st.Stop();
            Console.WriteLine($"Час сортування списку на 2000 елементів - {st.Elapsed.TotalMilliseconds}");
            linkedList.Clear();
            for (int i = 0; i < 5000; i++)
            {
                int newNode = rand.Next();
                linkedList.Add(newNode);
            }
            head = linkedList.GetHead();
            st = Stopwatch.StartNew();
            selectionList(head);
            st.Stop();
            Console.WriteLine($"Час сортування списку на 5000 елементів - {st.Elapsed.TotalMilliseconds}");
            linkedList.Clear();
            for (int i = 0; i < 10000; i++)
            {
                int newNode = rand.Next();
                linkedList.Add(newNode);
            }
            head = linkedList.GetHead();
            st = Stopwatch.StartNew();
            selectionList(head);
            st.Stop();
            Console.WriteLine($"Час сортування списку на 10000 елементів - {st.Elapsed.TotalMilliseconds}");
            Console.WriteLine("\n");
            Console.WriteLine("Сортування вставками (масив)");
            int[] arr = new int[10];
            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = rand.Next();
            }
            st = Stopwatch.StartNew();
            insertionArray(arr);
            st.Stop();
            Console.WriteLine($"Час сортування масиву на 10 елементів - {st.Elapsed.TotalMilliseconds}");
            arr = new int[100];
            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = rand.Next();
            }
            st = Stopwatch.StartNew();
            insertionArray(arr);
            st.Stop();
            Console.WriteLine($"Час сортування масиву на 100 елементів - {st.Elapsed.TotalMilliseconds}");
            arr = new int[500];
            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = rand.Next();
            }
            st = Stopwatch.StartNew();
            insertionArray(arr);
            st.Stop();
            Console.WriteLine($"Час сортування масиву на 500 елементів - {st.Elapsed.TotalMilliseconds}");
            arr = new int[1000];
            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = rand.Next();
            }
            st = Stopwatch.StartNew();
            insertionArray(arr);
            st.Stop();
            Console.WriteLine($"Час сортування  масиву на 1000 елементів - {st.Elapsed.TotalMilliseconds}");
            arr = new int[2000];
            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = rand.Next();
            }
            st = Stopwatch.StartNew();
            insertionArray(arr);
            st.Stop();
            Console.WriteLine($"Час сортування масиву на 2000 елементів - {st.Elapsed.TotalMilliseconds}");
            arr = new int[5000];
            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = rand.Next();
            }
            st = Stopwatch.StartNew();
            insertionArray(arr);
            st.Stop();
            Console.WriteLine($"Час сортування масиву на 5000 елементів - {st.Elapsed.TotalMilliseconds}");
            arr = new int[10000];
            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = rand.Next();
            }
            st = Stopwatch.StartNew();
            insertionArray(arr);
            st.Stop();
            Console.WriteLine($"Час сортування масиву на 10000 елементів - {st.Elapsed.TotalMilliseconds}");
            Console.WriteLine("\n");
            Console.WriteLine("Сортування вставками (список)");
            Console.WriteLine("\n");
            LinkedListSecond list = new LinkedListSecond();
            for (int i = 0; i < 10; i++)
            {
                int newNode = rand.Next();
                list.push(newNode);
            }
            st = Stopwatch.StartNew();
            list.insertionList(list.head);
            st.Stop();
            Console.WriteLine($"Час сортування списку на 10 елементів - {st.Elapsed.TotalMilliseconds}");
            list.сlear();
            for (int i = 0; i < 100; i++)
            {
                int newNode = rand.Next();
                list.push(newNode);
            }
            st = Stopwatch.StartNew();
            list.insertionList(list.head);
            st.Stop();
            Console.WriteLine($"Час сортування списку на 100 елементів - {st.Elapsed.TotalMilliseconds}");
            list.сlear();
            for (int i = 0; i < 500; i++)
            {
                int newNode = rand.Next();
                list.push(newNode);
            }
            st = Stopwatch.StartNew();
            list.insertionList(list.head);
            st.Stop();
            Console.WriteLine($"Час сортування списку на 500 елементів - {st.Elapsed.TotalMilliseconds}");
            list.сlear();
            for (int i = 0; i < 1000; i++)
            {
                int newNode = rand.Next();
                list.push(newNode);
            }
            st = Stopwatch.StartNew();
            list.insertionList(list.head);
            st.Stop();
            Console.WriteLine($"Час сортування списку на 1000 елементів - {st.Elapsed.TotalMilliseconds}");
            list.сlear();
            for (int i = 0; i < 2000; i++)
            {
                int newNode = rand.Next();
                list.push(newNode);
            }
            st = Stopwatch.StartNew();
            list.insertionList(list.head);
            st.Stop();
            Console.WriteLine($"Час сортування списку на 2000 елементів - {st.Elapsed.TotalMilliseconds}");
            list.сlear();
            for (int i = 0; i < 5000; i++)
            {
                int newNode = rand.Next();
                list.push(newNode);
            }
            st = Stopwatch.StartNew();
            list.insertionList(list.head);
            st.Stop();
            Console.WriteLine($"Час сортування списку на 5000 елементів - {st.Elapsed.TotalMilliseconds}");
            list.сlear();
            for (int i = 0; i < 10000; i++)
            {
                int newNode = rand.Next();
                list.push(newNode);
            }
            st = Stopwatch.StartNew();
            list.insertionList(list.head);
            st.Stop();
            Console.WriteLine($"Час сортування списку на 10000 елементів - {st.Elapsed.TotalMilliseconds}");
            Console.ReadKey();
        }
    }
}
